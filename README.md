<!--[![MasterHead](./images/Banner-4.jpg)](https://github.com/LucJosin) -->

<!-- https://user-images.githubusercontent.com/76869974/127777072-197eefdb-6185-41ef-ab6f-4cc759e9f37b.jpg -->

<!-- <h1 align="center"> <img src="https://emoji.gg/assets/emoji/7279-vibecat.gif" width="34"/> Welcome to my World 🌎 </h1> -->
<h1 align="center">
  <img src="https://emoji.gg/assets/emoji/7279-vibecat.gif" width="34"/>
  <a href="https://github.com/LucJosin">
   <img src="https://readme-typing-svg.herokuapp.com?font=Fredoka&color=0D1117&center=true&vCenter=true&width=280&height=20&lines=Hi!+I'm+Lucas+Josino+%F0%9F%98%80;Welcome+to+my+World+%F0%9F%8C%8D;lucasjosino.com+%7C+%40LucJosin+%F0%9F%8C%90">
  </a>
 <img src="./gifs/owl.gif" width="30"/>
</h1>

<p>About me:</p>

- I’m currently working on... 🤔

- I’m currently learning about <b>Mobile</b> with <b>Flutter/Dart</b> + <b>Kotlin</b> & <b>Swift</b>

- I’m looking to collaborate on <b>everything i can</b>

- I ❤️ 🦉

> ❝For in dreams we enter a world that is entirely our own. Let them swim in the deepest ocean or glide over the highest cloud.❞

<h3 align="left">Where to find me <small>(@LucJosin)</small>:</h3>
<p align="left">
<a href="https://linkedin.com/in/LucJosin" target="_blank">
  <img align="center" src="https://img.shields.io/badge/Linkedin-%23ffffff.svg?&style=for-the-badge&logo=Linkedin&logoColor=black" alt="Linkedin" />
</a>
<a href="https://github.com/LucJosin" target="_blank">
  <img align="center" src="https://img.shields.io/badge/GitHub-white?style=for-the-badge&logo=github&logoColor=black" alt="Github" />
</a>
<a href="https://pub.dev/publishers/lucasjosino.com/packages" target="_blank">
  <img align="center" src="https://img.shields.io/badge/Pub.dev-white?style=for-the-badge&logo=dart&logoColor=black" alt="Pubdev" />
</a>
<a href="https://twitter.com/LucJosin" target="_blank">
  <img align="center" src="https://img.shields.io/badge/Twitter-white?style=for-the-badge&logo=twitter&logoColor=black" alt="Twitter" />
</a>
 <a href="https://id.pinterest.com/LucJosin/" target="_blank">
  <img align="center" src="https://img.shields.io/badge/Pinterest-%23ffffff.svg?&style=for-the-badge&logo=Pinterest&logoColor=black" alt="Pinterest" />
 </a>
 <a href="https://stackoverflow.com/u/14500144/" target="_blank">
  <img align="center" src="https://img.shields.io/badge/Stack_Overflow-white?style=for-the-badge&logo=stack-overflow&logoColor=black" alt="StackOverFlow" />
 </a>
</p>

<h3 align="left">Website & Contact:</h3>
 <a href="https://www.lucasjosino.com/" target="_blank">
  <img align="center" src="https://img.shields.io/badge/-lucasjosino.com-white?style=for-the-badge&logo=data:image/svg%2bxml;base64,PHN2ZyBpZD0iQ2FtYWRhXzEiIGRhdGEtbmFtZT0iQ2FtYWRhIDEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmlld0JveD0iMCAwIDI1MS44OSAzMzIuOTMiPjxkZWZzPjxzdHlsZT4uY2xzLTF7c3Ryb2tlOiMwMDA7c3Ryb2tlLW1pdGVybGltaXQ6MTA7fTwvc3R5bGU+PC9kZWZzPjx0aXRsZT5sZy1kazwvdGl0bGU+PHBhdGggY2xhc3M9ImNscy0xIiBkPSJNNzY1LjM1LDUxNS4yNmwtMzAuNzYtNDAuODlhLjQ4LjQ4LDAsMCwxLS4wNi0uNDljOS42LTIxLjYsMjIuNDMtMzMuNzMsMjQuMjktMzUuNDFhLjQ5LjQ5LDAsMCwwLC4xMy0uNWwtMTEtMzYuMTZhLjQ4LjQ4LDAsMCwwLS45LDBjLTEwLjM5LDI0Ljg5LTM2LDMzLjc0LTM4Ljg2LDM0LjY2YS40My40MywwLDAsMS0uNDYtLjFjLTEyLjkzLTEyLjYtNDIuNjctNjcuNzEtNDQuNTgtNzEuMjZhLjQuNCwwLDAsMSwwLS4zN2MxLjI5LTMuMjQsMS4wNi05LjI5LDEtMTEuMDZhLjQ3LjQ3LDAsMCwwLS41My0uNDVjLTE0LjgsMS41My0xNi4yNiwxMy41Mi0xNi4zNywxNC42MmEuNDEuNDEsMCwwLDAsMCwuMTZsMy4xOCwxMS42OGEyMS4yNywyMS4yNywwLDAsMCwxLjkxLDQuNzVsNy44OSwxNC4zN0w2ODYuODIsNDQyYS40OC40OCwwLDAsMS0uMjkuNzJjLTI2LDYuMjYtNzkuNTQsMy43Mi04Ni4wOCwzLjM4YS40Ny40NywwLDAsMS0uNDEtLjY1YzIuMzUtNS43NywyMC45Mi01MSwzOS4zOC04MC42MywxOC4zLTI5LjQzLTEuODMtNDQuNjgtNC44OC00Ni43NmEuNDYuNDYsMCwwLDAtLjY0LjEyTDYyNiwzMjguNDlhLjQ5LjQ5LDAsMCwwLC4yOS43NmM2LjU0LDEuMzQtLjIsMTIuMzQtLjM2LDEyLjZoMEw1NzEuNDQsNDQyLjU3YS40OC40OCwwLDAsMS0uNDcuMjVjLTM1LjM4LTMuMTctNDQuNDUtNi45Mi00Ni4yOC03LjlhLjQ4LjQ4LDAsMCwwLS42NC4xOGwtOS40MywxNi4xNGEuNDcuNDcsMCwwLDAsMCwuNTNMNTQ1LjQ1LDQ5M2EuNDguNDgsMCwwLDEsMCwuNUM1MzUuMiw1MTUuMzMsNTIyLjgyLDUyNy4zLDUyMSw1MjlhLjQ3LjQ3LDAsMCwwLS4xMy41bDExLjkzLDM2LjU5Ljk1LDBjNi44Mi0yMy43NiwzNC4xOC0zMy45MiwzNy41Mi0zNS4wOGEuNDcuNDcsMCwwLDEsLjU0LjE4YzMuMjUsNC42Niw0NC44NCw2NC4zMiw0Niw3MS4yMWExOC44NCwxOC44NCwwLDAsMS0xLjE0LDEwLjgxLjQ3LjQ3LDAsMCwwLC40NS42N2MyOC45Mi0uOTIsMTAuNzgtMzQuODgtNS42OC02MC40NC04LjgtMTMuNjUtMTYuMjUtMjUtMTcuODItMjcuNDFhLjQ4LjQ4LDAsMCwxLC4xOS0uN0M2MTIsNTE3LDY3Myw1MjEuMSw2ODAuMjUsNTIxLjYzYS40OC40OCwwLDAsMSwuMzkuNjlMNjQyLjMsNTk5bDAsMGMtMjAuMzksMzQuMDkuOTQsNDguODEsNC4xMyw1MC43NmEuNDguNDgsMCwwLDAsLjY0LS4xNGw2LjktMTBhMS4zNSwxLjM1LDAsMCwwLS4xOS0xLjc2Yy0yLjExLTEuOTItMi4zMS0xMiwwLTExLjMzLDIuNjcuNzIsNTEuNTUtOTUuNTYsNTQuNi0xMDEuNThhLjUuNSwwLDAsMSwuNDktLjI3YzM3LjM2LDMuNDIsNDQuODIsNi4zOCw0Ni4zLDcuNWEuNDkuNDksMCwwLDAsLjcyLS4xNGw5LjQ4LTE2LjI0QS40Ny40NywwLDAsMCw3NjUuMzUsNTE1LjI2Wk01NjAuMTcsNDY2LjYybC0yLjQxLDUuMTVhLjc2Ljc2LDAsMCwxLTEuMjkuMTRsLTQtNS4xN2EuNjQuNjQsMCwwLDEsLjUtMWg2LjYyQS42NC42NCwwLDAsMSw1NjAuMTcsNDY2LjYyWm0xMzMuMTIsMzEuOWMtNDMtNS42Ny0xMTQsNS4xMi0xMTQsNS4xMkw1NzMsNDk1LjM5bDE0LjQyLTI2LjA5YzU2LDIuNDUsMTE0LjUtNS42MiwxMTQuNS01LjYybDUuMTcsOFptMzMuMTIsMi41NC00Ljc3LjM0YTEuMDYsMS4wNiwwLDAsMS0xLTEuNDVsMi00LjkxYTEsMSwwLDAsMSwxLjg3LS4xNWwyLjc3LDQuNTdBMS4wNSwxLjA1LDAsMCwxLDcyNi40MSw1MDEuMDZaIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtNTE0LjA1IC0zMTcuNDMpIi8+PC9zdmc+&logoColor=black&link=mailto:lucasjosino.com" alt="Website" /> 
 </a>
 <a href="mailto:contact@lucasjosino.com">
  <img align="center" src="https://img.shields.io/badge/-contact@lucasjosino.com-white?style=for-the-badge&logo=Mail.Ru&logoColor=black&link=mailto:contact@lucasjosino.com" alt="Contact" />
 </a>

<h3 align="left">Languages, Tools & Stats:</h3>
<p align="left">
 <a href="https://dart.dev/" target="_blank">
  <img align="center" src="https://img.shields.io/badge/Dart-white?style=for-the-badge&logo=dart&logoColor=black" alt="Flutter" height="30" width="90" />
 </a>
 <a href="https://flutter.dev/" target="_blank">
  <img align="center" src="https://img.shields.io/badge/Flutter-white?style=for-the-badge&logo=flutter&logoColor=black" alt="Flutter" height="30" width="100" />
 </a>
 <a href="https://kotlinlang.org/" target="_blank">
  <img align="center" src="https://img.shields.io/badge/Kotlin-white?&style=for-the-badge&logo=kotlin&logoColor=black" height="30" width="100" />
 </a>
 <a href="https://www.apple.com/swift/" target="_blank">
  <img align="center" src="https://img.shields.io/badge/Swift-white?&style=for-the-badge&logo=swift&logoColor=black" alt="Swift" height="30" width="90" />
 </a>
 <a href="/">
  <img align="center" src="https://img.shields.io/badge/|-white?&style=for-the-badge" alt="Others languages/tools i like to use" height="30" width="30" />
 </a>
 <a href="https://www.w3.org/html/" target="_blank">
  <img align="center" src="https://img.shields.io/badge/HTML-white?style=for-the-badge&logo=html5&logoColor=black" alt="HTML5" height="30" width="100" />
 </a>
 <a href="https://www.w3schools.com/css/" target="_blank">
  <img align="center" src="https://img.shields.io/badge/CSS-white?&style=for-the-badge&logo=css3&logoColor=black" alt="CSS" height="30" width="90" />
 </a>
 <a href="https://www.javascript.com/" target="_blank">
  <img align="center" src="https://img.shields.io/badge/JavaScript-white?&style=for-the-badge&logo=javascript&logoColor=black" alt="JavaScript" height="30" width="140" />
 </a>
</p>
